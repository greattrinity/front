import React from 'react'
import Header from './Components/HeaderAuth'
import AddRForm from './Components/AddRForm'
class Add extends React.Component{
    
    render() { 
        return(
            <div className="signIn">
            <Header/>
                <div className="container">
                    <AddRForm/>
            </div>
        </div>
        )
    }
}

export default Add;