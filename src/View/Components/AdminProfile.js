import React from 'react'
import { withTranslation } from 'react-i18next';
import CleaningCard from './AdminClCard';
import AdminCustCart from './AdminCustCart';


class Profile extends React.Component{

    render() {
        localStorage.removeItem("Email")
        localStorage.removeItem("Role")
            const {t} = this.props
            return(
                <div>
                    <div className="profile_back">
                        <p id="cName">{t("Admin")}</p>
                    </div>
                    <div className="rooms_back">
                        <p id="EMP">{t("CComp")}</p>
                    </div>
                        <div id="rooms_container">
                           <CleaningCard/>
                        </div>
                        <div className="rooms_back">
                            <p id="EMP">{t("Comp")}</p>
                        </div>
                        <div id="room_container">
                           <AdminCustCart/>
                        </div>
                </div>
            )
        }
    
}

export default withTranslation()  (Profile);