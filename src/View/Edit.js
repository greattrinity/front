import React from 'react'
import Header from './Components/HeaderAuth'
import EditForm from './Components/EditForm'
class Edit extends React.Component{
    
    render() { 
        return(
            <div className="signIn">
            <Header/>
                <div className="container">
                    <EditForm/>
            </div>
        </div>
        )
    }
}

export default Edit;