import React from 'react'
import Header from './Components/HeaderAuth'
import EditRoomForm from './Components/EditRoomForm'
class Edit extends React.Component{
    
    render() { 
        return(
            <div className="signIn">
            <Header/>
                <div className="container">
                    <EditRoomForm/>
            </div>
        </div>
        )
    }
}

export default Edit;