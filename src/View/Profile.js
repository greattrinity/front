import React from 'react'
import Header from './Components/HeaderAuth'
import jwt_decode from "jwt-decode"
import CompanyProfile from './Components/CompanyProfile'
import CleaningProfile from './Components/CleaningProfile'
import AdminProfile from './Components/AdminProfile'

if(localStorage.getItem("Token") != null){
    var token = localStorage.getItem("Token")
    var decoded = jwt_decode(token)
}

class Profile extends React.Component{

    render() {

        if(localStorage.getItem("Token") == null){
            window.location.href='./'
        }else{
            if(decoded.role === "CUSTOMER_COMPANY"){
                return(
                    <div className="profile">
                        <Header/>
                        <CompanyProfile/>
                    </div>
                )
            } else if(decoded.role === "CLEANING_COMPANY"){
                return(
                    <div className="profile">
                        <Header/>
                        <CleaningProfile/>
                    </div>
                )
            }
            else if(decoded.role === "ADMIN"){
                return(
                    <div className="profile">
                        <Header/>
                        <AdminProfile/>
                    </div>
                )
            }
        }
    }
}

export default Profile;